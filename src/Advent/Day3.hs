{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE OverloadedStrings #-}

module Advent.Day3 where

import qualified Control.Foldl as Fold
import qualified Data.Set as Set
import qualified Data.Text as T
import Turtle

data Result = Result { xpos :: Int, ypos :: Int, seen :: Set.Set (Int, Int) }

data Pair = Pair { a :: Result, b :: Result, onA :: Bool }

emptyResult :: Result
emptyResult = Result { xpos = 0, ypos = 0, seen = Set.singleton (0, 0) }

emptyPair :: Pair
emptyPair = Pair { a = emptyResult, b = emptyResult, onA = True }

uniqueResult :: Result -> Int
uniqueResult r = Set.size $ seen r

uniquePair :: Pair -> Int
uniquePair p = Set.size $ Set.union (seen (a p)) (seen (b p))

acc :: Result -> Char -> Result
acc (Result { xpos, ypos, seen }) c = Result { xpos = nextX, ypos = nextY, seen = nextSeen }
  where
    nextX =
      case c of
        '<' -> xpos - 1
        '>' -> xpos + 1
        _ -> xpos
    nextY =
      case c of
        '^' -> ypos + 1
        'v' -> ypos - 1
        _ -> ypos
    nextSeen = Set.insert (nextX, nextY) seen

acc2 :: Pair -> Char -> Pair
acc2 (Pair { a, b, onA }) c = Pair { a = nextA, b = nextB, onA = not onA }
  where
    nextA = if onA then acc a c else a
    nextB = if onA then b else acc b c

outerFold :: Fold.Fold Text Int
outerFold = Fold.Fold (\x t -> T.foldl' acc x t) emptyResult uniqueResult

outerFold2 :: Fold.Fold Text Int
outerFold2 = Fold.Fold (\x t -> T.foldl' acc2 x t) emptyPair uniquePair

day3 :: Shell Text -> IO Int
day3 inp = fold inp outerFold

answer :: IO Int
answer = day3 $ input "static/day3/input.txt"