{-# LANGUAGE OverloadedStrings #-}

module Main where

import qualified Advent.Day1 as Day1
import qualified Advent.Day2 as Day2
import qualified Advent.Day3 as Day3
import qualified Advent.Day4 as Day4
import qualified Advent.Day5 as Day5
import qualified Advent.Day6 as Day6
import Test.HUnit.Base
import Test.HUnit.Text
import Turtle

day1_1 :: Test
day1_1 = TestCase $ do
  actual <- Day1.floorNum <$> (Day1.day1 $ return "(()(()(")
  assertEqual "day1_1" actual 3

day1_answer :: Test
day1_answer = TestCase $ do
  actual <- Day1.answer
  assertEqual "day1_answer" actual 74

day1_answer2 :: Test
day1_answer2 = TestCase $ do
  actual <- Day1.answer2
  assertEqual "day1_answer2" actual (Just 1795)

day2_1 :: Test
day2_1 = TestCase $ assertEqual "day2_1" (Day2.area 2 3 4) 58

day2_2 :: Test
day2_2 = TestCase $ assertEqual "day2_2" (Day2.area 1 1 10) 43

day2_answer :: Test
day2_answer = TestCase $ do
  actual <- Day2.answer
  assertEqual "day2_answer" actual 1606483

day3_answer :: Test
day3_answer = TestCase $ do
  actual <- Day3.answer
  assertEqual "day3_answer" actual 2572

day4_answer :: Test
day4_answer = TestCase $ assertEqual "day4_answer" (Day4.answer) (Just 346386)

niceTest :: Text -> Test
niceTest text = TestCase $ do
  actual <- Day5.day5 $ return text
  assertEqual "nice" actual 1

naughtyTest :: Text -> Test
naughtyTest text = TestCase $ do
  actual <- Day5.day5 $ return text
  assertEqual "naughty" actual 0

allTests :: Test
allTests = TestList [niceTest "ugknbfddgicrmopn", naughtyTest "jchzalrnumimnmhp",
                     naughtyTest "haegwjzuvuyypxyu", naughtyTest "dvszwmarrgswjxmb"]

main :: IO Counts
main = runTestTT allTests
