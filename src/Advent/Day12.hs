{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}

module Advent.Day12 where

import Control.Applicative (many)
import Control.Exception.Base (assert)
import qualified Control.Foldl as Fold
import Data.Bits
import Data.Char (chr, ord)
import Data.List (foldl')
import qualified Data.Map.Strict as Map
import Data.Maybe (isJust)
import Data.Monoid
import Data.Profunctor (lmap)
import qualified Data.Set as Set
import qualified Data.Text as T
import Debug.Trace
import Text.Heredoc
import Turtle

data JSON = JNumber Int | JString String | JArray [JSON] | JObject [(String, JSON)] deriving (Show)

parseJSON :: Pattern JSON
parseJSON = parseJNumber <|> parseJString <|> parseJArray <|> parseJObject
  where
    parseJNumber = JNumber <$> signed decimal
    parseJString = JString <$> parseString
    parseJArray = JArray <$> (between (char '[') (char ']') (parseJSON `sepBy` char ','))
    parseJObject = JObject <$> (between (char '{') (char '}') (parseField `sepBy` char ','))
    parseString = do
      _ <- char '"'
      name <- many letter
      _ <- char '"'
      return name
    parseField = do
      name <- parseString
      _ <- char ':'
      value <- parseJSON
      return (name, value)

sumNums :: JSON -> Int
sumNums = go 0
  where
    go :: Int -> JSON -> Int
    go i (JNumber j) = i + j
    go i (JString _) = i
    go i (JArray js) = foldl' go i js
    go i (JObject js) = foldl' go i (snd <$> js)

sumNums2 :: JSON -> Int
sumNums2 = go 0
  where
    go :: Int -> JSON -> Int
    go i (JNumber j) = i + j
    go i (JString _) = i
    go i (JArray js) = foldl' go i js
    go i (JObject js) = if hasRed js then i else (foldl' go i (snd <$> js))
    hasRed [] = False
    hasRed ((_, x):xs) =
      case x of
        JString "red" -> True
        _ -> hasRed xs

run :: Text -> Maybe Int
run s =
  case match (parseJSON <* eof) s of
    [] -> Nothing
    (x:_) -> Just $ sumNums2 x

firstMatching :: (a -> Bool) -> Fold a (Maybe a)
firstMatching f = Fold acc Nothing id
  where acc Nothing v | f v = Just v
                      | otherwise = Nothing
        acc j _ = j

answer :: IO (Maybe Int)
answer = join <$> fold (run <$> (input "static/day12/input.txt")) (firstMatching isJust)