{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}

module Advent.Day7 where

import Data.Profunctor (lmap)
import Data.List (foldl')
import Debug.Trace
import Data.Bits
import qualified Control.Foldl as Fold
import qualified Data.Set as Set
import qualified Data.Map.Strict as Map
import qualified Data.Text as T
import Text.Heredoc
import Turtle

data Id = NameId String | LiteralId Int deriving (Show)
data UnaryOp = OpNot deriving (Show)
data BinaryOp = OpOr | OpAnd | OpLshift | OpRshift deriving (Show)
data Gate = LiteralGate Int | NameGate String | UnaryGate UnaryOp Id | BinaryGate BinaryOp Id Id deriving (Show)
type Wire = (String, Gate)
type Defn = Map.Map String Gate
type State = Map.Map String Int
type Order = Map.Map String (Set.Set String)

type M = Either String

idDeps :: Id -> [String]
idDeps (NameId s) = [s]
idDeps _ = []

gateDeps :: Gate -> [String]
gateDeps (UnaryGate _ id) = idDeps id
gateDeps (BinaryGate _ id1 id2) = idDeps id1 ++ idDeps id2
gateDeps (LiteralGate _) = []
gateDeps (NameGate name) = [name]

orderDeps :: Order -> String -> [String]
orderDeps order name =
  case (Map.lookup name order) of
    Just s -> Set.toList s
    _ -> []

topoSort :: Order -> [String]
topoSort order = reverse $ snd $ foldl' acc (Set.empty, []) (Map.keys order)
  where
    acc :: (Set.Set String, [String]) -> String -> (Set.Set String, [String])
    acc p@(seen, emitted) name
      | Set.member name seen = p
      | otherwise = (nextSeen, name:nextEmitted)
        where
          deps = orderDeps order name
          (nextSeen, nextEmitted) = foldl' acc ((Set.insert name seen), emitted) deps

topoSortDefn :: Defn -> [String]
topoSortDefn defn = topoSort (traceShowId finalOrder)
  where
    updateOrder :: Order -> (String, Gate) -> Order
    updateOrder order (name, gate) = Map.insert name (Set.fromList (gateDeps gate)) order
    finalOrder :: Order
    finalOrder = foldl' updateOrder (Map.empty) (Map.toList defn)

evaluateAll :: Defn -> State -> M State
evaluateAll defn state =
  let
    order = traceShowId (topoSortDefn (traceShowId (defn)))
  in foldl' (\s n -> case s of { Right x -> evaluateSub defn x n; Left s -> Left s }) (Right state) order

resolve :: State -> Id -> M Int
resolve state (NameId name) = case (Map.lookup name state) of { Just x -> Right x; _ -> Left ("resolve: " <> name) }
resolve state (LiteralId value) = Right value

store :: State -> String -> Int -> State
store state name value = Map.insert name value state

evaluateSub :: Defn -> State -> String -> M State
evaluateSub defn state name = do
  gate <- case (Map.lookup name defn) of { Just x -> Right x; _ -> Left ("evaluateSub: " <> name) }
  case gate of
    LiteralGate value ->
      return $ store state name value
    NameGate name2 -> do
      value <- resolve state (NameId name2)
      return $ store state name value
    UnaryGate op id -> do
      value <- resolve state id
      return $ store state name $ case op of
        OpNot -> complement value
    BinaryGate op id1 id2 -> do
      value1 <- resolve state id1
      value2 <- resolve state id2
      return $ store state name $ case op of
        OpOr -> value1 .|. value2
        OpAnd -> value1 .&. value2
        OpLshift -> shiftL value1 value2
        OpRshift -> shiftR value1 value2

foldToMap :: (Ord k) => Fold (k, v) (Map.Map k v)
foldToMap = Fold (\m (k, v) -> Map.insert k v m) Map.empty id

parseId :: Pattern Id
parseId = (LiteralId <$> decimal) <|> ((NameId . T.unpack) <$> (plus letter))

parseLiteral :: Pattern Gate
parseLiteral = LiteralGate <$> decimal

parseName :: Pattern Gate
parseName = (NameGate . T.unpack) <$> (plus letter)

parseUnaryOp :: Pattern UnaryOp
parseUnaryOp = (const OpNot) <$> (text "NOT")

parseUnary :: Pattern Gate
parseUnary = do
  op <- parseUnaryOp
  _ <- space
  id <- parseId
  return $ UnaryGate op id

parseBinaryOp :: Pattern BinaryOp
parseBinaryOp =
      (const OpOr) <$> (text "OR")
  <|> (const OpAnd) <$> (text "AND")
  <|> (const OpLshift) <$> (text "LSHIFT")
  <|> (const OpRshift) <$> (text "RSHIFT")

parseBinary :: Pattern Gate
parseBinary = do
  id1 <- parseId
  _ <- space
  op <- parseBinaryOp
  _ <- space
  id2 <- parseId
  return $ BinaryGate op id1 id2

parseGate :: Pattern Gate
parseGate = parseLiteral <|> parseName <|> parseUnary <|> parseBinary

pattern :: Pattern Wire
pattern = do
 gate <- parseGate
 _ <- text " -> "
 name <- plus letter
 return $ ((T.unpack name), gate)

day7 :: Shell Text -> IO (Defn, M State)
day7 inp = (\d -> (d, evaluateAll d Map.empty)) <$> defn
  where
    defn :: IO Defn
    defn = fold (inp >>= select . match pattern) foldToMap

testData :: Shell Text
testData =
  let
    here :: String
    here =
      [str|123 -> x
          |456 -> y
          |x AND y -> d
          |x OR y -> e
          |x LSHIFT 2 -> f
          |y RSHIFT 2 -> g
          |NOT x -> h
          |NOT y -> i
          |
          |]
    ls :: [Text]
    ls = T.pack <$> (lines here)
  in select ls

test7 :: IO (M State)
test7 = snd <$> day7 testData

answer7_2 :: IO (Maybe Int)
answer7_2 = do
  (defn, mstate) <- day7 $ input "static/day7/input.txt"
  return $ case mstate of
    Right x ->
      case Map.lookup "a" x of
        Just ans ->
          let updatedDefn = Map.insert "b" (LiteralGate ans) defn
          in case (evaluateAll updatedDefn x) of
            Right y -> Map.lookup "a" y
            _-> Nothing
        _ -> Nothing
    _ -> Nothing

answer7 :: IO (Maybe Int)
answer7 = do
  (defn, mstate) <- day7 $ input "static/day7/input.txt"
  return $ case mstate of
    Right x -> Map.lookup "a" x
    _ -> Nothing
