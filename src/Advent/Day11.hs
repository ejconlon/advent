{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}

module Advent.Day11 where

import Control.Applicative (many)
import Control.Exception.Base (assert)
import qualified Control.Foldl as Fold
import Data.Bits
import Data.Char (chr, ord)
import Data.List (foldl')
import qualified Data.Map.Strict as Map
import Data.Monoid
import Data.Profunctor (lmap)
import qualified Data.Set as Set
import qualified Data.Text as T
import Debug.Trace
import Text.Heredoc
import Turtle

aord :: Int
aord = ord 'a'

toInt :: String -> Int
toInt s = go 0 s
  where
    go :: Int -> String -> Int
    go i [] = i
    go i (x:xs) = go ((ord x) - aord + 26 * i) xs

toStr :: Int -> String
toStr i = pad $ go [] i
  where
    go :: String -> Int -> String
    go xs 0 = xs
    go xs i = go ((chr (aord + (i `mod` 26))):xs) (i `div` 26)

    pad :: String -> String
    pad s | length s < 8 = pad ('a':s)
          | otherwise = s

nextPassword :: String -> String
nextPassword = until isValid incr

hasIncreasing :: String -> Bool
hasIncreasing s = undefined

noMistaken :: String -> Bool
noMistaken s = undefined

hasDoubles :: String -> Bool
hasDoubles s = undefined

isValid :: String -> Bool
isValid s = hasIncreasing s && noMistaken s && hasDoubles s

incr :: String -> String
incr = toStr . (+1) . toInt

tryId :: String
tryId = toStr (toInt "abcdefgh")

test1 :: String
test1 = nextPassword "abcdefgh"

test2 :: String
test2 = nextPassword "ghijklmn"

answer :: String
answer = nextPassword "vzbxkghb"