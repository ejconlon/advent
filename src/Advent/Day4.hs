{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE OverloadedStrings #-}

module Advent.Day4 where

import qualified Control.Foldl as Fold
import qualified Data.ByteString as B
import qualified Data.ByteString.Lazy as L
import qualified Data.Digest.Pure.MD5 as MD5
import qualified Data.Set as Set
import qualified Data.Text as T
import qualified Data.Text.Encoding as E
import Turtle

hash :: Text -> Int -> Text
hash key val = T.pack $ show (MD5.md5 (L.fromStrict bs))
  where
    bs :: B.ByteString
    bs = E.encodeUtf8 (key <> (T.pack (show val)))

fiveZeros :: Text -> Bool
fiveZeros = T.isPrefixOf "000000"

myKey :: Text
myKey = "iwrupvqb"

first :: (a -> Bool) -> [a] -> Maybe a
first _ [] = Nothing
first f (x:xs) | f x = Just x
               | otherwise = first f xs

answer :: Maybe Int
answer = first (fiveZeros . (hash myKey)) [0..]