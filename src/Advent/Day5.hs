{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE OverloadedStrings #-}

module Advent.Day5 where

import Debug.Trace
import qualified Control.Foldl as Fold
import qualified Data.Set as Set
import qualified Data.Text as T
import Turtle

vowels :: Set.Set Char
vowels = Set.fromList "aeiou"

-- It contains at least three vowels (aeiou only), like aei, xazegov, or aeiouaeiouaeiou.
three :: Text -> Bool
three text = numVowels >= 3
  where
    numVowels = T.foldl' (\n e -> if Set.member e vowels then n + 1 else n) 0 text

-- It contains at least one letter that appears twice in a row, like xx, abcdde (dd), or aabbccdd (aa, bb, cc, or dd).

data Acc = AccStart | AccLast Char | AccFound deriving Eq

twice :: Text -> Bool
twice text = (result == AccFound)
  where
    result :: Acc
    result = T.foldl' f AccStart text
    f :: Acc -> Char -> Acc
    f (AccStart) c = AccLast c
    f (AccLast last) c | last == c = AccFound
                       | otherwise = AccLast c
    f (AccFound) _ = AccFound

-- It does not contain the strings ab, cd, pq, or xy, even if they are part of one of the other requirements.
hasBad :: Text -> Bool
hasBad text = any (\i -> T.isInfixOf (T.pack i) text) ["ab", "cd", "pq", "xy"]

nice :: Text -> Bool
nice s =
  let t = (three s)
      u = (twice s)
      v = (not (hasBad s))
    in t && u && v

nice2 :: Text -> Bool
nice2 s =
  let t = doubleDouble s
      u = repeatGap s
    in t && u

data Projection = Projection Int Text deriving (Show)

ddp :: [Projection] -> Bool
ddp ps = any id $ do
  (Projection i ab) <- ps
  (Projection j cd) <- ps
  return $ (ab == cd) && (i - j > 1)

projectN :: Int -> Text -> [Projection]
projectN n s = let l = T.length s in do
  (a, i) <- T.tails s `zip` [0..]
  if i <= l - n then return (Projection i (T.take n a)) else []

doubleDouble :: Text -> Bool
doubleDouble s = ddp (projectN 2 s)

repeatGap :: Text -> Bool
repeatGap s = any (\(Projection _ t) -> (T.head t) == (T.last t)) (projectN 3 s)

shellFilter :: (a -> Bool) -> Shell a -> Shell a
shellFilter f as = do
  a <- as
  if f a then return a else empty

day5 :: Shell Text -> IO Int
day5 inp = fold (shellFilter nice inp) Fold.length

answer5 :: IO Int
answer5 = day5 $ input "static/day5/input.txt"