{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE OverloadedStrings #-}

module Advent.Day2 where

import qualified Control.Foldl as Fold
import qualified Data.Text as T
import Turtle

uncurry3 :: (a -> b -> c -> d) -> (a, b, c) -> d
uncurry3 f (a, b, c) = f a b c

area :: Int -> Int -> Int -> Int
area l w h = 2 * (l * w + l * h + w * h) + (minimum [l * w, l * h, w * h])

rib :: Int -> Int -> Int -> Int
rib l w h = 2 * (minimum [(l + w), (l + h), (w + h)]) + l * w * h

parse :: Pattern (Int, Int, Int)
parse = do
  l <- decimal
  _ <- char 'x'
  w <- decimal
  _ <- char 'x'
  h <- decimal
  return (l, w, h)

day2 :: Shell Text -> IO Int
day2 inp = fold inp2 Fold.sum
  where
    inp2 :: Shell Int
    inp2 = (\i -> uncurry3 area $ head $ match parse i) <$> inp

answer :: IO Int
answer = day2 $ input "static/day2/input.txt"
