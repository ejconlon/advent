{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}

module Advent.Day10 where

import Control.Applicative (many)
import Control.Exception.Base (assert)
import qualified Control.Foldl as Fold
import Data.Bits
import Data.List (foldl')
import qualified Data.Map.Strict as Map
import Data.Monoid
import Data.Profunctor (lmap)
import qualified Data.Set as Set
import qualified Data.Text as T
import Debug.Trace
import Text.Heredoc
import Turtle

explode :: Int -> [Int]
explode num = go num []
  where
    go num xs | num == 0 = xs
              | otherwise = go (num `div` 10) ((num `mod` 10):xs)

flatten :: [(Int, Int)] -> [Int]
flatten xs = do
  (a, b) <- xs
  (explode a) ++ [b]

xform :: [Int] -> [(Int, Int)]
xform [] = []
xform (x:xs) = go 1 x xs
  where
    go :: Int -> Int -> [Int] -> [(Int, Int)]
    go count value [] = [(count, value)]
    go count value (next:ys) | value == next = go (count + 1) value ys
                             | otherwise = (count, value):(go 1 next ys)

step :: [Int] -> [Int]
step = flatten . xform

day :: Int -> Int -> Int
day num iter = length $ go 0 (explode num)
  where
    go :: Int -> [Int] -> [Int]
    go i xs | i == iter = xs
            | otherwise = go (i + 1) (step xs)

test :: Int
test = day 1 5

answer :: Int
answer = day 3113322113 50