{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}

module Advent.Day8 where

import Control.Applicative (many)
import qualified Control.Foldl as Fold
import Data.Bits
import Data.List (foldl')
import qualified Data.Map.Strict as Map
import Data.Monoid
import Data.Profunctor (lmap)
import qualified Data.Set as Set
import qualified Data.Text as T
import Debug.Trace
import Text.Heredoc
import Turtle

data Elem = ElemQuote | ElemChar | EscapeQuote | EscapeSlash | EscapeHex deriving (Show, Eq)

internalElemPattern :: Pattern Elem
internalElemPattern =
      (const ElemChar <$> letter)
  <|> (const EscapeQuote <$> ((char '\\') >> (char '"')))
  <|> (const EscapeSlash <$> ((char '\\') >> (char '\\')))
  <|> (const EscapeHex <$> ((char '\\') >> (char 'x') >> alphaNum >> alphaNum))

strLen :: Fold.Fold Elem (Sum Int)
strLen = Fold.foldMap project id
  where
    project elem =
      case elem of
        ElemQuote -> 0
        _ -> 1

codeLen :: Fold.Fold Elem (Sum Int)
codeLen = Fold.foldMap project id
  where
    project elem =
      case elem of
        ElemQuote -> 1
        ElemChar -> 1
        EscapeQuote -> 2
        EscapeSlash -> 2
        EscapeHex -> 4

explodeAll :: [Elem] -> [Elem]
explodeAll elems = (ElemQuote : ElemQuote : (elems >>= explode))

explode :: Elem -> [Elem]
explode elem =
  case elem of
    ElemQuote -> [EscapeQuote]
    ElemChar -> [ElemChar]
    EscapeQuote -> [EscapeSlash, EscapeQuote]
    EscapeSlash -> [EscapeSlash, EscapeSlash]
    EscapeHex -> [EscapeSlash, ElemChar, ElemChar, ElemChar]

foldList :: (Foldable f, Monoid b) => Fold.Fold a b -> Fold.Fold (f a) b
foldList f = Fold.foldMap project id
  where project elem = Fold.fold f elem

linePattern :: Pattern [Elem]
linePattern = do
  startQuo <- const ElemQuote <$> char '"'
  elems <- many internalElemPattern
  endQuo <- const ElemQuote <$> char '"'
  _ <- eof
  return $ explodeAll (startQuo : endQuo : elems)

consume :: Shell Text -> Pattern a -> Shell a
consume lines pattern = do
  line <- lines
  select $ match pattern line

day :: Shell Text -> IO Int
day lines = do
  (x, y) <- (fold (consume lines linePattern) (foldList $ (,) <$> strLen <*> codeLen))
  return $ (getSum y) - (getSum x)

testData :: Shell Text
testData =
  let
    here :: String
    here =
      [str|""
          |"abc"
          |"aaa\"aaa"
          |"\x27"
          |
          |]
    ls :: [Text]
    ls = T.pack <$> (lines here)
  in select ls

test :: IO Int
test = day testData

answer :: IO Int
answer = day $ input "static/day8/input.txt"
