{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE OverloadedStrings #-}

module Advent.Day6 where

import Data.Profunctor (lmap)
import Data.List (foldl')
import Debug.Trace
import qualified Control.Foldl as Fold
import qualified Data.Set as Set
import qualified Data.Map.Strict as Map
import qualified Data.Text as T
import Turtle

data Turn = TurnOn | TurnOff | Toggle deriving (Show, Eq)
data Command = Command Turn Int Int Int Int deriving (Show, Eq)

turnPattern :: Pattern Turn
turnPattern = toggle <|> turnOn <|> turnOff
  where
    toggle = const Toggle <$> text "toggle"
    turnOn = const TurnOn <$> text "turn on"
    turnOff = const TurnOff <$> text "turn off"

pattern :: Pattern Command
pattern = do
  turn <- turnPattern
  _ <- space
  a <- decimal
  _ <- char ','
  b <- decimal
  _ <- text " through "
  c <- decimal
  _ <- char ','
  d <- decimal
  return $ Command turn a b c d

type Lights = Map.Map (Int, Int) Int

emptyArray :: Int -> Lights
emptyArray i = Map.empty

numLit :: Lights -> Int
numLit lights = foldl' (\v (_, i) -> v + i) 0 $ Map.toList lights

subUpdate :: Int -> Turn -> Int
subUpdate i TurnOn = i + 1
subUpdate i TurnOff = max 0 (i - 1)
subUpdate i Toggle = i + 2

update :: Lights -> Command -> Lights
update state (Command turn a b c d) =
  let
    updateRow :: Int -> Int -> Int -> Int
    updateRow i j v = if (i >= a && i <= c && j >= b && j <= d) then subUpdate v turn else v
    acc :: Lights -> (Int, Int) -> Lights
    acc prev p@(i, j) =
      let v = updateRow i j (Map.findWithDefault 0 p prev) in
        Map.insert p v prev
    span :: [(Int, Int)]
    span = [(i, j) | i <- [a..c], j <- [b..d]]
  in foldl' acc state span

myFold :: Int -> Fold.Fold Command Int
myFold i = Fold.Fold (\a c -> update a (traceShowId c)) (emptyArray i) numLit

day6 :: Shell Text -> IO Int
day6 inp = fold inp2 (myFold 1000)
  where
    inp2 :: Shell Command
    inp2 = do
      i <- inp
      let c = match pattern i in
        case c of
          [] -> empty
          (x:_) -> return x

answer6 :: IO Int
answer6 = day6 $ input "static/day6/input.txt"