{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE OverloadedStrings #-}

module Advent.Day1 where

import qualified Control.Foldl as Fold
import qualified Data.Text as T
import Turtle

data Result = Result { floorNum :: Int, index :: Int, negOneIndex :: Maybe Int }

emptyResult :: Result
emptyResult = Result { floorNum = 0, index = 0, negOneIndex = Nothing }


acc :: Result -> Char -> Result
acc (Result { floorNum, index, negOneIndex }) c = Result { floorNum = nextFloor, index = nextIndex, negOneIndex = nextNegOneIndex }
  where
    nextFloor =
      case c of
        '(' -> floorNum + 1
        ')' -> floorNum - 1
        _ -> floorNum
    nextIndex = index + 1
    nextNegOneIndex =
      case negOneIndex of
        Nothing ->
          case nextFloor of
            -1 -> Just nextIndex
            _ -> Nothing
        _ -> negOneIndex

outerFold :: Fold.Fold Text Result
outerFold = Fold.Fold (\x t -> T.foldl' acc x t) emptyResult id

day1 :: Shell Text -> IO Result
day1 inp = fold inp outerFold

answer :: IO Int
answer = floorNum <$> (day1 $ input "static/day1/input.txt")

answer2 :: IO (Maybe Int)
answer2 = negOneIndex <$> (day1 $ input "static/day1/input.txt")