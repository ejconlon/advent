{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}

module Advent.Day9 where

import Control.Applicative (many)
import Control.Exception.Base (assert)
import qualified Control.Foldl as Fold
import Data.Bits
import Data.List (foldl')
import qualified Data.Map.Strict as Map
import Data.Monoid
import Data.Profunctor (lmap)
import qualified Data.Set as Set
import qualified Data.Text as T
import Debug.Trace
import Text.Heredoc
import Turtle

type Table = Map.Map String (Map.Map String Int)

makeTable :: Fold.Fold (String, String, Int) Table
makeTable = Fold.Fold (\t (a, b, c) -> insertTable a b c t) Map.empty id

insertTable :: String -> String -> Int -> Table -> Table
insertTable a b c t = insertSingle b a c $ insertSingle a b c t

insertSingle :: String -> String -> Int -> Table -> Table
insertSingle a b c t = Map.insert a u t
  where
    u = maybe (Map.singleton b c) (Map.insert b c) (Map.lookup a t)

pattern :: Pattern (String, String, Int)
pattern = do
  a <- many letter
  _ <- text " to "
  b <- many letter
  _ <- text " = "
  d <- decimal
  _ <- eof
  return (a, b, d)

consume :: Shell Text -> Pattern a -> Shell a
consume lines pattern = do
  line <- lines
  select $ match pattern line

process :: Table -> Int
process table = maximum (cost table <$> routes table)

pairs :: [String] -> [(String, String)]
pairs [] = []
pairs [a] = []
pairs (x:y:zs) = (x, y):(pairs (y:zs))

cost :: Table -> [String] -> Int
cost table route = foldl' acc 0 $ pairs route
  where
    acc i (a, b) = i + j
      where
        j = (maybe 0 id (Map.lookup a table >>= Map.lookup b ))
        --k = assert ((length route) == Map.size table) j

iterateTable :: String -> Table -> [String]
iterateTable a t = maybe [] (Map.keys) (Map.lookup a t)

routes :: Table -> [[String]]
routes table =
  do
    start <- places
    go (Set.fromList places) start [start]
  where
    places :: [String]
    places = Map.keys table
    go :: Set.Set String -> String -> [String] -> [[String]]
    go unseen cur xs | 1 == Set.size unseen = [xs]
                     | otherwise =
                      let
                        unseen2 = Set.delete cur unseen
                      in do
                        chosen <- filter (\i -> Set.member i unseen2) (iterateTable cur table)
                        go unseen2 chosen (chosen:xs)

day :: Shell Text -> IO Int
day lines = process <$> fold (consume lines pattern) makeTable

testData :: Shell Text
testData =
  let
    here :: String
    here =
      [str|London to Dublin = 464
          |London to Belfast = 518
          |Dublin to Belfast = 141
          |]
    ls :: [Text]
    ls = T.pack <$> (lines here)
  in select ls

test :: IO Int
test = day testData

answer :: IO Int
answer = day $ input "static/day9/input.txt"
